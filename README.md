# Lottery Simulator

This project is a lottery simulator.

## Requirements

- Install Node.js LTS version: https://nodejs.org/en/download/package-manager
- Install pnpm: https://pnpm.io/installation

You shouldn't need to install npm or anything else.

## How to run

1. Clone this repository
2. Run `pnpm install`
3. Run `pnpm build`
4. Run `pnpm start --tickets=10 --draws=3 --max=10 --logLevel=info --interval=3000`

   - `tickets` is the number of tickets to buy
   - `draws` is the number of draws
   - `max` is the maximum number for each draw number
   - `logLevel` is the log level to show (trace, debug, info, warn, error), info is the default and probably enough
   - `interval` is the interval between draws in milliseconds, helpful to see the draws happening when picking a small draw number

5. You can also run `pnpm test` to run the tests, which is a fixed lottery draw with 10 tickets and 3 draws.

When running the main `pnpm start` command you will see the draws happening and the winners being announced for each combination.

If you want to run big simulations be aware:

- 10 millions tickets: `node --max-old-space-size=16384 dist/index.js "--tickets=10000000" "--draws=6" "--max=49"` takes about 9s
- 100 millions `node --max-old-space-size=16384 dist/index.js "--tickets=10000000" "--draws=6" "--max=49"` takes about 10m
- If you dare trying one billion tickets, you will need to increase the memory limit and it will take a long time

## How it works

The lottery state machine runs the lottery drawing. You can visualise the simple machine by visiting this [link](https://stately.ai/registry/editor/a162382c-8d75-42c7-a956-285c202251ec?machineId=7bb31a70-a277-4ac4-b941-5e12c141864e).

Some tickets are created with a factory function and then the lottery starts.
On each draw the winners are calculate.

The main algorithm is in the calculateWinners function, it uses a simple data structure to group numbers and the tickets that have them, then it calculates the winners for each combination.
The way that is calculates is by traversing the matrix of ticket ids by incrementing each column index by one when the ticket id is the minimum of the row of ids. More than one coloumn can be the minimum ( same number ), and how many columns have that same minumum is the number of winners for that size combination.

Example:

5, 3, 3, 7, 10

3 is the minimum, so the second and third columns are incremented by one, and the number of winners for the combination of size 2 is 2.

Please see the log messages to understand the process and play with smaller size draws and numbers of tickets.

## Optimisations

You can see in `src/calculateWinners.ts` that there is a small optimisation, which on 10 millions tickets saved about 23 second.

```typescript
const winningTicketsByNumbers: WinningTicketByNumber[] = [];

function calculateWinners(
```

Caching the winning tickets by number by using the Javascript concept of scope saves some time, instead of re-calculate the grouping of numbers and tickets with that number.

At the beginning I had also a guid, alongside the ticket id, but I removed it as it saved some memory on big numbers of tickets.
