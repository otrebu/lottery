import minimist from "minimist";
import { hrtime } from "process";
import { createActor } from "xstate";
import { createLotteryTickets } from "./data-factory.js";
import logger from "./logger.js";
import lotteryMachine from "./lottery-machine.js";
import readline from "node:readline";

const args = minimist(process.argv.slice(2));

logger.debug(args, "Command line arguments: ");

// Use the command line arguments or default values
const numberOfTickets = args.tickets || 1_000_000;
const drawsTotal = args.draws || 6;
const maxNumber = args.max || 49;
const intervalBetweenDrawsInMs = args.interval || 1;

logger.info(
  `Number of tickets: ${numberOfTickets.toLocaleString()}\nNumber of draws: ${drawsTotal}\nMax number: ${maxNumber}\n`,
);

const tickets = createLotteryTickets(numberOfTickets, drawsTotal, maxNumber);

logger.info(`Created ${tickets.length.toLocaleString()} lottery tickets\n`);
logger.debug(tickets, "Tickets: ");
logger.info("Starting lottery ❗");

const startLotteryTimestamp = hrtime.bigint();
const actorLottery = createActor(lotteryMachine, {
  input: { drawsTotal, tickets, intervalBetweenDrawsInMs, maxNumber },
});

actorLottery.subscribe((snapshot) => {
  logger.info(`🤖 Lottery machine state: ${snapshot.value}\n`);
  if (snapshot.value === "end") {
    const endLotteryTimestamp = hrtime.bigint();
    logger.info(
      `⏰ Execution time: ${(endLotteryTimestamp - startLotteryTimestamp) / BigInt(1_000_000)} ms`,
    );
  }
});

actorLottery.start();

actorLottery.send({ type: "start" });

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

rl.question(`Press enter to close the application...\n`, () => {
  actorLottery.stop();
  rl.close();
});
