export interface LotteryTicket {
  id: number;
  numbers: number[];
}

export interface LotteryDraw {
  tickets: LotteryTicket[];
  drawsTotal: number;
  draws: number[];
  winners: unknown[];
  intervelBetweenDrawsInMs: number;
  maxNumber: number;
}

export interface WinningTicketByNumber {
  draw: number;
  ticketsIncludingDraw: LotteryTicket[];
}

export interface WinnerCount {
  howMany: number;
  ticketIds: number[];
}
