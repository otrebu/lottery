import { assign, fromPromise, setup } from "xstate";
import calculateWinners from "./calculateWinners.js";
import { generateRandomNumber } from "./data-factory.js";
import logger from "./logger.js";
import { LotteryDraw, LotteryTicket } from "./types.js";
import { sleep } from "./utils.js";

const lotteryMachine = setup({
  types: {
    context: {} as LotteryDraw,
    events: {} as { type: "start" },
    input: {} as {
      drawsTotal: number;
      tickets: LotteryTicket[];
      intervalBetweenDrawsInMs: number;
      maxNumber: number;
    },
  },
  actions: {
    calculateWinners: function ({ context, event }, params) {
      logger.debug({ context, event }, "Calculating winners 🏆");
      calculateWinners(context.tickets, context.draws);
    },
    addDraw: assign(({ context, event }) => {
      logger.trace({ event }, "Adding draw to context ✏️");
      return {
        ...context,
        // @ts-ignore
        draws: [...context.draws, event.output],
      };
    }),
    logDraws: function ({ context, event }) {
      logger.info(`🎱 Lottery draws: ${context.draws} 🎱\n`);
    },
  },
  actors: {
    drawNumber: fromPromise<
      number,
      { draws: number[]; intervalBetweenDrawsInMs: number; maxNumber: number }
    >(async ({ input }) => {
      const { draws, intervalBetweenDrawsInMs, maxNumber } = input;
      let draw = generateRandomNumber(1, maxNumber);
      logger.info(`Drawn number: ${draw} 🎲`);
      logger.info(`Drawn numbers so far: ${draws} 📚`);
      while (draws.includes(draw)) {
        logger.warn({ draw }, "Number already drawn, drawing again 🔁");
        draw = generateRandomNumber(1, 10);
        logger.warn({ draw }, "Drawn number");
      }
      await sleep(intervalBetweenDrawsInMs);
      return draw;
    }),
  },
  guards: {
    checkIsLastDraw: function ({ context, event }) {
      const { drawsTotal, draws } = context;
      logger.debug(
        { drawsTotal, drawsCount: draws.length },
        "Checking if last draw ❓🔚",
      );
      return draws.length === drawsTotal;
    },
  },
}).createMachine({
  context: ({ input }) => ({
    tickets: input.tickets,
    drawsTotal: input.drawsTotal,
    draws: [],
    winners: [],
    intervelBetweenDrawsInMs: input.intervalBetweenDrawsInMs,
    maxNumber: input.maxNumber,
  }),
  id: "lottery",
  initial: "idle",
  states: {
    idle: {
      on: {
        start: {
          target: "drawing",
        },
      },
    },
    drawing: {
      invoke: {
        onDone: {
          actions: {
            // @ts-ignore
            type: "addDraw",
          },
          target: "drawn",
        },
        input: ({ context }) => ({
          draws: context.draws,
          intervalBetweenDrawsInMs: context.intervelBetweenDrawsInMs,
          maxNumber: context.maxNumber,
        }),
        src: "drawNumber",
      },
    },
    drawn: {
      always: [
        {
          target: "end",
          actions: {
            // @ts-ignore
            type: "calculateWinners",
          },
          guard: {
            type: "checkIsLastDraw",
          },
        },
        {
          target: "drawing",
          actions: {
            // @ts-ignore
            type: "calculateWinners",
          },
        },
      ],
    },
    end: {
      type: "final",
      entry: {
        // @ts-ignore
        type: "logDraws",
      },
    },
  },
});

export default lotteryMachine;
