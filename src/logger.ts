import minimist from "minimist";
import { pino } from "pino";

const args = minimist(process.argv.slice(2));
const logLevel = args.logLevel || "info";

const logger = pino({
  level: logLevel, // Set the log level to 'info'
  transport: {
    target: "pino-pretty", // Use the 'pino-pretty' transport
    options: {
      colorize: true, // Colorize the output
      ignore: "pid,hostname", // Ignore the 'pid' and 'hostname' fields
    },
  },
});

export default logger;
