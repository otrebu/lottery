import { expect, suite } from "vitest";
import calculateWinners from "./../calculateWinners.js";
import { eachDrawWinnersCount, lotteryDraw, tickets } from "./fixtures.js";
import { hrtime } from "process";

suite("Lottery", (test) => {
  test("Winners calculation", () => {
    const currentDraws: number[] = [];

    for (let drawIndex = 0; drawIndex < lotteryDraw.length; drawIndex++) {
      const draw = lotteryDraw[drawIndex];

      if (draw === undefined) {
        throw new Error("Draw is undefined");
      }

      currentDraws.push(draw);

      console.log(`Draw: ${draw}`);
      console.log(`Current draws: ${currentDraws}`);

      // Act
      const startCalculateWinnersTimestamp = hrtime.bigint();
      const winnersByNumber = calculateWinners(tickets, currentDraws);
      const endCalculateWinnersTimestamp = hrtime.bigint();

      console.log(
        `Execution time: ${(endCalculateWinnersTimestamp - startCalculateWinnersTimestamp) / BigInt(1_000_000)} ms`,
      );

      // Assert
      expect(winnersByNumber).toBeDefined();
      expect(winnersByNumber[0]).toBeDefined();
      expect(winnersByNumber[0]!.howMany).toBe(1);
      expect(winnersByNumber[0]!.ticketIds).toEqual(
        eachDrawWinnersCount[drawIndex]![0]!.ticketIds,
      );

      if (drawIndex === 1) {
        expect(winnersByNumber[1]).toBeDefined();
        expect(winnersByNumber[1]!.howMany).toBe(2);
        expect(winnersByNumber[1]!.ticketIds).toEqual(
          eachDrawWinnersCount[drawIndex]![1]!.ticketIds,
        );
      }

      if (drawIndex === 2) {
        expect(winnersByNumber[2]).toBeDefined();
        expect(winnersByNumber[2]!.howMany).toBe(3);
        expect(winnersByNumber[2]!.ticketIds).toEqual(
          eachDrawWinnersCount[drawIndex]![2]!.ticketIds,
        );
      }
    }
  });
});
