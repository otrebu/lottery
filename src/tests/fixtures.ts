import { LotteryTicket, WinnerCount } from "../types.js";

const tickets: LotteryTicket[] = [
  {
    id: 0,
    numbers: [1, 6, 9],
  },
  {
    id: 1,
    numbers: [9, 2, 3],
  },
  {
    id: 2,
    numbers: [9, 7, 6],
  },
  {
    id: 3,
    numbers: [6, 10, 4],
  },
  {
    id: 4,
    numbers: [7, 6, 8],
  },
  {
    id: 5,
    numbers: [7, 4, 5],
  },
  {
    id: 6,
    numbers: [5, 8, 3],
  },
  {
    id: 7,
    numbers: [9, 8, 2],
  },
  {
    id: 8,
    numbers: [8, 1, 3],
  },
  {
    id: 9,
    numbers: [10, 6, 7],
  },
];

const lotteryDraw = [6, 10, 7];

const eachDrawWinnersCount: WinnerCount[][] = [
  [
    {
      howMany: 1,
      ticketIds: [0, 2, 3, 4, 9],
    },
  ],
  [
    {
      howMany: 1,
      ticketIds: [0, 2, 4],
    },
    {
      howMany: 2,
      ticketIds: [3, 9],
    },
  ],
  [
    {
      howMany: 1,
      ticketIds: [0, 5],
    },
    {
      howMany: 2,
      ticketIds: [2, 3, 4],
    },
    {
      howMany: 3,
      ticketIds: [9],
    },
  ],
];
export { tickets, lotteryDraw, eachDrawWinnersCount };
