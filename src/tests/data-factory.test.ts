import { expect, suite, test } from "vitest";
import {
  createLotteryTickets,
  generateRandomNumber,
} from "./../data-factory.js";

suite("data-factory", () => {
  test("generateRandomNumber", () => {
    const min = 1;
    const max = 10;
    const num = generateRandomNumber(min, max);
    expect(num).toBeGreaterThanOrEqual(min);

    expect(num).toBeLessThanOrEqual(max);
  });

  test("createLotteryTickets", () => {
    const count = 10;
    const drawsTotal = 3;
    const maxNumber = 10;
    const tickets = createLotteryTickets(count, drawsTotal, maxNumber);
    expect(tickets.length).toBe(count);
    tickets.forEach((ticket) => {
      expect(ticket.numbers.length).toBe(drawsTotal);
      expect(ticket.id).toBeGreaterThanOrEqual(0);
    });
  });
});
