import { randomInt } from "crypto";
import { LotteryTicket } from "./types.js";

export function generateRandomNumber(min: number, max: number) {
  const num = randomInt(min, max + 1);
  return num;
}

export function createLotteryTicket(
  id: number,
  drawsTotal: number,
  maxNumber: number,
): LotteryTicket {
  const numbers: number[] = [];
  while (numbers.length < drawsTotal) {
    const randomNumber = generateRandomNumber(1, maxNumber);
    if (!numbers.includes(randomNumber)) {
      numbers.push(randomNumber);
    }
  }
  return {
    id,
    numbers,
  };
}

export function createLotteryTickets(
  count: number,
  drawsTotal = 6,
  maxNumber = 49,
): LotteryTicket[] {
  return Array.from({ length: count }, (_, i) =>
    createLotteryTicket(i, drawsTotal, maxNumber),
  );
}
