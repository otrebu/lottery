import logger from "./logger.js";
import { LotteryTicket, WinnerCount, WinningTicketByNumber } from "./types.js";

const winningTicketsByNumbers: WinningTicketByNumber[] = [];

function calculateWinners(
  tickets: LotteryTicket[],
  lotteryDraws: number[],
): WinnerCount[] {
  // Avoiding to recaclulate the same winning tickets on 10 millions tickets and 6 draws and max 49 numbers saves about 22 seconds

  // const winningTicketsByNumbers: WinningTicketByNumber[] = lotteryDraws.reduce(
  //   (accumulator: WinningTicketByNumber[], draw) => {
  //     const ticketsIncludingDraw = tickets.filter((ticket) =>
  //       ticket.numbers.includes(draw),
  //     );
  //     return [
  //       ...accumulator,
  //       {
  //         draw,
  //         ticketsIncludingDraw,
  //       },
  //     ];
  //   },
  //   [],
  // );

  lotteryDraws.forEach((draw) => {
    if (!winningTicketsByNumbers.some((ticket) => ticket.draw === draw)) {
      const ticketsIncludingDraw = tickets.filter((ticket) =>
        ticket.numbers.includes(draw),
      );
      winningTicketsByNumbers.push({
        draw,
        ticketsIncludingDraw,
      });
    }
  });

  logger.debug(winningTicketsByNumbers, "Winning tickets by numbers:");

  const winnersCount: WinnerCount[] = lotteryDraws.map((_, index) => {
    return {
      howMany: index + 1,
      ticketIds: [],
    };
  });

  let isCountingWinners = true;
  const rowIndexes: number[] = new Array(lotteryDraws.length).fill(0);

  while (isCountingWinners) {
    const rowOfTicketIds = winningTicketsByNumbers.map(
      (winningTicketByNumber, index) => {
        const currentRowIndex = rowIndexes[index];
        if (currentRowIndex === undefined) {
          throw new Error("currentRowIndex is undefined");
        }
        const ticket =
          winningTicketByNumber.ticketsIncludingDraw[currentRowIndex];

        return ticket?.id ?? undefined;
      },
    );

    logger.debug(`Row of ticket IDs: ${rowOfTicketIds}`);

    const minTicketId = Math.min(
      ...(rowOfTicketIds.filter((id) => id !== undefined) as number[]),
    );
    logger.debug(`Minimum ticket ID: ${minTicketId}`);

    const minTicketIndexes = rowOfTicketIds.reduce(
      (indexes: number[], id, index) => {
        if (id === minTicketId) {
          indexes.push(index);
        }
        return indexes;
      },
      [],
    );

    logger.debug(`Indexes of tickets with minimum ID: ${minTicketIndexes}`);

    const howManyWinners = minTicketIndexes.length;

    if (howManyWinners > winnersCount.length) {
      throw new Error("Too many winners");
    }

    // Add ticket IDs to the winners count
    winnersCount[howManyWinners - 1]?.ticketIds.push(minTicketId);

    // Move the indexes
    for (const minTicketIndex of minTicketIndexes) {
      rowIndexes[minTicketIndex]++;
    }

    const nextRowOfTicketIds = winningTicketsByNumbers.map(
      (winningTicketByNumber, index) => {
        const currentRowIndex = rowIndexes[index];
        if (currentRowIndex === undefined) {
          throw new Error("currentRowIndex is undefined");
        }
        const ticket =
          winningTicketByNumber.ticketsIncludingDraw[currentRowIndex];

        return ticket?.id ?? undefined;
      },
    );

    if (nextRowOfTicketIds.every((id) => id === undefined)) {
      isCountingWinners = false;
    }
  }

  logger.info("🎉 Winners count: 🎉");

  for (const winnerCount of winnersCount) {
    logger.info(
      `${winnerCount.ticketIds.length.toLocaleString()} winners by ${winnerCount.howMany.toLocaleString()} combination`,
    );
  }

  logger.info(`💰\n`);

  return winnersCount;
}

export default calculateWinners;
